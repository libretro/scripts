#!/bin/bash

CMSHIELD_URL="http://libretro.luceoscutum.org";
LIBRETRO_URL="http://buildbot.libretro.com";
CORE_PATH="nightly/android/latest/arm64-v8a";
STRIP_EXT="_libretro_android.so.zip";

comm -13 \
  <(curl -s ${CMSHIELD_URL}/${CORE_PATH}/ \
    |grep -o 'href=".*">' \
    |sed 's/href="//;s/\/">//' \
    |awk -F\" '{ print $1 }' \
    |grep 'so.zip' \
    |sed "s/${STRIP_EXT}//" \
    |sort\
  ) \
  <(curl -s ${LIBRETRO_URL}/${CORE_PATH}/ \
    |tr ' ' '\n' \
    |grep 'so.zip' \
    |awk -F\> '{ print $2 }' \
    |awk -F \< '{ print $1 }' \
    |sed "s/${STRIP_EXT}//" \
    |sort\
  )
