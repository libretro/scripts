import gitlab

exclude_list = [
  "bsnes hd",         # Renamed from github bsnes-hd
  "ep128emu",         # Renamed from github ep128emu-core
  "flycast upstream", # Multiple copies of the same core
  "fmsx",             # Renamed from github fmsx-libretro
  "jumpnbump",        # Renamed from github jumpnbump-libretro
  "kronos",           # Part of github yabause
  "libretro-uae2021", # Part of github libretro-uae
  "lowresnx",         # Renamed from github lowres-nx
  "lutro",            # Renamed from github libretro-lutro
  "play",             # Renamed from github Play-
  "vaporspec",        # Renamed from github vm
  "yabasanshiro",     # Part of github yabause
]

cmshield = gitlab.Gitlab('https://gitlab.incom.co')
libretro = gitlab.Gitlab('https://git.libretro.com')

cprojects = cmshield.groups.get('libretro').projects.list(all=True,order_by='name',sort='asc')
lprojects = libretro.groups.get('libretro').projects.list(all=True,order_by='name',sort='asc')

cprojnames = []
for cproj in cprojects:
    cprojnames.append(cmshield.projects.get(cproj.id).name.lower())

lprojnames = []
for lproj in lprojects:
    lprojnames.append(libretro.projects.get(lproj.id).name.lower())

tempprojects = list(set(lprojnames).difference(cprojnames))
missingprojects = list(set(tempprojects).difference(exclude_list))

for proj in sorted(missingprojects):
    print(proj)
