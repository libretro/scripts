#!/bin/bash

if [ -z "${CI_PROJECT_DIR}" ]; then
	# Find top level working directory
	SOURCE="${BASH_SOURCE[0]}";
	while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
		INFRADIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )";
		SOURCE="$(readlink "$SOURCE")";
		[[ $SOURCE != /* ]] && SOURCE="$INFRADIR/$SOURCE"; # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
	done;
	SCRIPTSDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )";
	export CI_PROJECT_DIR="$(dirname $(dirname "${SCRIPTSDIR}"))";
fi;

pushd "${CI_PROJECT_DIR}"
repo forall -i '^infra' -c 'CHANGE_COUNT=$(git rev-list --count FORK-$REPO_RREV..incom/$REPO_RREV); if [ ${CHANGE_COUNT} -gt 2 ]; then echo -n $REPO_PATH": "${CHANGE_COUNT}; fi;'
popd
