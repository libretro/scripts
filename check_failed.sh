#!/bin/bash

declare -a FAILED_CORES=();

while read corename status; do
  if [ "$status" == "Rebase failed" ]; then
    FAILED_CORES+=("${corename%:*}");
  fi;
done < sync.log;

rm sync.log;

if [ ! ${#FAILED_CORES[@]} -eq 0 ]; then
  echo "${#FAILED_CORES[@]} failed repos:";
  for core in "${FAILED_CORES[@]}"; do
    echo "  - ${core}"
  done;

  exit 1;
fi;
