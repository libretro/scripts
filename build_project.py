import gitlab
import sys

if len(sys.argv) <= 3:
    print("Not enough arguments")
    sys.exit(2)

cmshield = gitlab.Gitlab('https://gitlab.incom.co', private_token=str(sys.argv[1]))

project = cmshield.projects.get(str(sys.argv[2]).split(".")[0], lazy=True)
pipeline = project.pipelines.create({'ref': str(sys.argv[3]), 'variables': []})

print("Building " + str(sys.argv[2]) + ":" + str(sys.argv[3]))
