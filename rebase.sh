#!/bin/bash

REPO_REMOTE=$1;
REPO_RREV=$2;
UPSTREAM_REMOTE=upstream;

if ! git remote get-url upstream > /dev/null 2>&1; then
  while read -r remote repourl; do
    git remote add $remote $repourl;
    git fetch $remote;
  done < .gitupstream;
  if ! git remote get-url upstream > /dev/null 2>&1; then
    echo "$(basename $(pwd)): No upstream to rebase on";
    exit 0;
  fi;
else
  git fetch ${UPSTREAM_REMOTE};
fi;

if ! git rev-parse FORK-$REPO_RREV > /dev/null 2>&1; then
  echo "$(basename $(pwd)): Creating tag";
  git tag FORK-$REPO_RREV ${UPSTREAM_REMOTE}/${REPO_RREV};
  git push ${REPO_REMOTE} FORK-$REPO_RREV -o ci.skip
elif [ "$(git rev-parse FORK-$REPO_RREV)" != "$(git rev-parse ${UPSTREAM_REMOTE}/${REPO_RREV})" ]; then
  fail=0;
  git checkout ${UPSTREAM_REMOTE}/${REPO_RREV} > /dev/null 2>&1;
  if ! git cherry-pick FORK-$REPO_RREV..${REPO_REMOTE}/${REPO_RREV} > /dev/null 2>&1; then
    if ! git diff -s --exit-code .gitlab-ci.yml; then
      git checkout ${REPO_REMOTE}/${REPO_RREV} .gitlab-ci.yml;
      if ! git -c core.editor=true cherry-pick --continue > /dev/null 2>&1; then
        fail=1;
      fi;
    else
      fail=1;
    fi;
  fi;

  if [ ! $fail -eq 0 ]; then
    echo "$(basename $(pwd)): Rebase failed";
    git cherry-pick --abort > /dev/null 2>&1;
  else
    echo "$(basename $(pwd)): Pushing update";
    git tag -d FORK-$REPO_RREV > /dev/null 2>&1;
    git push --delete ${REPO_REMOTE} FORK-$REPO_RREV > /dev/null 2>&1;
    git tag FORK-$REPO_RREV ${UPSTREAM_REMOTE}/${REPO_RREV};
    git push ${REPO_REMOTE} FORK-$REPO_RREV -o ci.skip
    git push --force ${REPO_REMOTE} HEAD:${REPO_RREV};
  fi;
#else
#  echo "$(basename $(pwd)): Up to date";
fi;
